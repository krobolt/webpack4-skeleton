/**
 *
 */
class Menu {

    /**
     * Start Loop on start
     */
    constructor(){
        $('.menu li').click(function(){
            $('.menu').addClass('active');
            $('.menu li').removeClass('active');
            $('.menu ul').removeClass('active');
        });

        $('#viewport').click(function(){
            $('.menu').removeClass('active');
            $('.menu li').removeClass('active');
        });

        $('.menu li').mouseover(function(){
            var ele =  $(this).parent('ul');
            ele.addClass('active');
        });

        $('.parent').mouseover(function(){
            $('.menu ul').removeClass('active');
        });

        $('.window').click(function(){
            $(this).toggleClass('active');
        })
    }


}

export {Menu as default};




