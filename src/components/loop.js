/**
 * Create event.
 * JQuery equivalent
 * @param eventName
 */
let createEvent = (eventName) => {
    if (document.createEvent) {
        event = document.createEvent("HTMLEvents");
        event.initEvent(eventName, true, true);
    } else {
        event = document.createEventObject();
        event.eventType = eventName;
    }
    event.eventName = eventName;

    if (document.createEvent) {
        document.dispatchEvent(event);
    } else {
        document.fireEvent("on" + event.eventType, event);
    }
}

/**
 *
 */
class AppLoop {

    /**
     * Start Loop on start
     */
    constructor(){
        this.running = false;
        this.rate = 2000;
        this.boot();
    }

    /**
     * Watch Event Loop
     */
    boot(){

        let self = this;
        createEvent('beforeTick');

        //start the running if it's not running.
        if (self.running === false){
            self.restart();
        }

        createEvent('tick');
        createEvent('afterTick');
    }

    /**
     * Stop Main Loop
     */
    stop(){
        console.log('..stoping');
        clearInterval(this.running);
    }

    /**
     * Start Main Loop
     * @param rate
     */
    start(rate){
        console.log('starting...');
        this.running = setInterval(this.boot, rate);
    }

    /**
     * Restart Loop
     * @param rate
     */
    restart(rate){
        console.log('restarting');
        if(typeof rate === "undefined"){
            rate = this.rate;
        }
        this.stop();
        this.start(rate);
    }
}

export {AppLoop as default};




