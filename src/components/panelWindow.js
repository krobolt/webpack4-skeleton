/**
 *
 */
class PanelWindow {

    /**
     * Start Loop on start
     */
    constructor(){

        var windowsState = {
            scale : 2,
            x: 697,
            y: 360,
            over: false
        };

        $('#main-window').mousemove(function (e) {

            windowsState.over = true;

            if(e.buttons === 1 || e.buttons === 3){
                console.log(e.clientY);
                console.log(e.clientX);
                console.log('...');
                $('#documentA').find('.window-page').css("transform", "translateX("+ e.clientX+"px) translateY("+ e.clientY +"px) scale(2)")
            }
        })

        $('#main-window').on('wheel', function(event){
            let win = $(this);
            let view = win.find('.window-viewport');
            function setScale(ele, dif){
                let scale = parseInt(ele.attr('data-scale'));
                let v = scale + dif;
                v = v < 0 ? 0 : v;
                v = v > 10 ? 10 : v;
                view.attr('data-scale',v);
            }
            if(event.originalEvent.deltaY < 0){
                setScale(view,1);
            }
            else {
                setScale(view,-1);
            }
        });


    }


}

export {PanelWindow as default};




